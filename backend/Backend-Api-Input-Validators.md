# Walidacja danych wejściowych po stronie serwera
Serwer waliduje dane wejściowe. Poniżej zamieszczono listę wyrażeń regularnych, które są używane przez serwer. <br>
| Typ                       | Wyrażenie regularne                                         |
|---------------------------|:------------------------------------------------------------|
| Login                     | `^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$`       |
| Hasło                     | `^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$`                    |
| JWT                       | `^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$`   |
| Token urządzenia          | `^[A-Za-z0-9]{16}$`                                         |
| Adres MAC                 | `^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$`                 |
| Id urządzenia             | `^\d{1,10}$`                                                |
| Nazwa urządzenia          | `^[a-zA-ZąĄćĆęĘłŁńŃóÓśŚźŹżŻ 0-9\-_]{5,200}$`                |
| Liczba zminnoprzecinkowa  | `^[+-]?([0-9]*[.])?[0-9]+$`                                 |
| Liczba całkowita          | `^[+-]?[0-9]+$`                                             |