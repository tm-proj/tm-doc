# Konfiguracja aplikacji serwerowej
Konfiguracja jest możliwa poprzez utworzenie pliku `app/app-conf.ini`.<br>
Plik ten nie jest dołączony do repozytorium.<br>
Oczekiwana struktura pliku:

```ini
#
# @file		app-conf.ini
# @brief	Structure of the configuration file for the backend app
# @author	Dominik Kala
# @date		2021-04-18
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

[app]
# This configuration will be overwritten if env var PORT is set
port = 80                       # Port to listen for http connections

[logs]
# If this flag is set to 1, the debugging information will be printed on the console
debug = 0                       # Run app in debug mode
directory = logs                # Directory to save logs in

[db]
# Some of these parameters can be overwritten by env vars
host = localhost                # DB host address,  MYSQL_HOST overwrites it
port = 3306                     # DB host port,     MYSQL_PORT overwrites it
user = user                     # DB user name,     MYSQL_USER overwrites it
password = password             # DB user password, MYSQL_USER_PASS overwrites it
database = db                   # Database name,    MYSQL_DB overwrites it

[credentials]
keyPath = privateKeyPath.key    # Private key path
certPath = certPath.crt         # Certificate path
caPath = ca_bundle.crt          # Trusted CA certificate
```