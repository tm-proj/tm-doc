Spis treści:
- [1. Endpointy Backendu](#1-endpointy-backendu)
- [2. Aplikacja moblina](#2-aplikacja-moblina)
  - [2.1. Rejestracja nowego użytkownika](#21-rejestracja-nowego-użytkownika)
  - [2.2. Logowanie użytkownika](#22-logowanie-użytkownika)
  - [2.3. Pobieranie danych z urządzeń użytkownika](#23-pobieranie-danych-z-urządzeń-użytkownika)
  - [2.4. Lista urządzeń użytkownika](#24-lista-urządzeń-użytkownika)
  - [2.5. Preinicjalizacja urządzenia](#25-preinicjalizacja-urządzenia)
  - [2.6. Usunięcie urządzenia](#26-usunięcie-urządzenia)
- [3. Urządzenie](#3-urządzenie)
  - [3.1. Inicjalizacja](#31-inicjalizacja)
  - [3.2. Zapis pomiarów](#32-zapis-pomiarów)
- [4. Statusy HTTP](#4-statusy-http)
  

# 1. Endpointy Backendu
Dokument ten definiuje EP backendu oraz struktury danych wymienianych pomiędzy serwerem a klientem. EP podzielono ze względu klienta dla którego są przeznaczone, tj. aplikacja lub urządzenie. <br>

- Aplikacja mobilna
  -  Rejestracja nowego użytkownika
  -  Logowanie użytkownika
  -  Pobieranie danych z urządzeń użytkownika
  -  Preinicjalizacja urządzenia
  -  Usunięcie urządzenia
- Urządzenie
  -  Inicjalizacja 
  -  Zapis pomiarów

# 2. Aplikacja moblina
## 2.1. Rejestracja nowego użytkownika
EP: `mobile/auth/signup`
<br><br>
Klient -> Serwer:<br>
Metoda: `POST`<br>
Dane:
```json
{
  "login": "string",
  "pass": "password"
}
```
Serwer -> Klient:<br>
Kod HTTP: `201` - Ok.<br>
Kod HTTP: `409` - Login użyty przez innego użytkownika.<br>
Dane:
```json
{
  "token": "accessToken"
}
```
Jeżeli kod HTTP nie jest wartością `201` to `token` nie zostanie wysłany.<br>

## 2.2. Logowanie użytkownika
EP: `mobile/auth/signin`
<br><br>
Klient -> Serwer:<br>
Metoda: `POST`<br>
Dane:
```json
{
  "login": "string",
  "pass": "password"
}
```
Serwer -> Klient:<br>
Kod HTTP: `200` - Ok.<br>
Kod HTTP: `401` - Nie udało sie zalogować - błędne dane.<br>
Dane:
```json
{
  "token": "accessToken"
}
```
Jeżeli kod HTTP nie jest wartością `201` to `token` nie zostanie wysłany.<br>

## 2.3. Pobieranie danych z urządzeń użytkownika
EP: `mobile/get-measures`
<br><br>
Klient -> Serwer:<br>
Metoda: `POST`<br>
Dodatkowy nagłówek:<br>
```HTTP
x-access-token: xAccessToken
```
Dane:
```json
{
  "deviceIds": 
  [

  ],
  "latestNum": uint32_t
}
```
Jeżeli tablica `deviceIds` nie zostanie przekazana, zwrócone zostaną pomiary dla wszystkich urządzeń użytkownika. <br>
Jeżeli atrybut `latestNum` nie zostanie przekazany, zwrócone zostaną najnowsze pomiary dla każdego urządzenia (jeden na urządzenie). <br>

Serwer -> Klient:<br>

Kod HTTP: `200` - Ok.<br>
Dane:
```json
{
  "devices":
  [
    {
      "deviceId": uint32_t,
      "name": "device name",
      "measures":
      [
        {
          "utcUnixTimestamp": uint32_t,
          "temperature": float,
          "humidity": float,
          "pm2_5": float,
          "pm10": float
        }
      ]
    }
  ]
}
```
Jeżeli użytkownik nie posiada żadnych przypisanych urządzeń, to zwracana jest pusta tablica `devices`.

## 2.4. Lista urządzeń użytkownika
EP: `mobile/get-devices`
<br><br>
Klient -> Serwer:<br>
Metoda: `GET`<br>
Dodatkowy nagłówek:<br>
```HTTP
x-access-token: xAccessToken
```
Serwer -> Klient:<br>
Kod HTTP: `200` - Ok.<br>
Dane:
```json
{
  "devices":
  [
    {
      "deviceId": uint32_t,
      "name": "device name"
    }
  ]
}
```
Jeżeli użytkownik nie posiada żadnych przypisanych urządzeń, to zwracana jest pusta tablica `devices`.

## 2.5. Preinicjalizacja urządzenia
EP: `mobile/preinit-device`
<br><br>
Klient -> Serwer:<br>
Metoda: `POST`<br>
Dodatkowy nagłówek:<br>
```HTTP
x-access-token: xAccessToken
```
Dane:
```json
{
  "name": "device name"
}
```
Serwer -> Klient:<br>
Kod HTTP: `201` - Ok.<br>
Kod HTTP: `409` - Użytkownik posiada już urządzenie o podanej nazwie.<br>
Dane:
```json
{
  "deviceAccessToken": string
}
```
Paramter `deviceAccessToken` to liczba 64 bitowa zapisana jako hex string.<br>
Jeżeli kod HTTP nie jest wartością `201` to `deviceAccessToken` nie zostanie wysłany.<br>

## 2.6. Usunięcie urządzenia
EP: `mobile/delete-device`
<br><br>
Klient -> Serwer:<br>
Metoda: `DELETE`<br>
Dodatkowy nagłówek:<br>
```HTTP
x-access-token: xAccessToken
```
Dane:
```json
{
  "deviceId": uint32_t
}
```

Serwer -> Klient:<br>
Kod HTTP: `200` - Ok.<br>
Kod HTTP : `404` - jeżeli urządzenie o podanym `deviceId` nie istanieje, lub nie należy do użytkownika identyfikującego się za pomocą `x-access-token`.<br>

# 3. Urządzenie
## 3.1. Inicjalizacja 
EP: `device/init`
<br><br>
Klient -> Serwer:<br>
Metoda: `POST`<br>
Dodatkowy nagłówek:<br>
```HTTP
device-access-token: uint64_t
```
Dane:
```json
{
  "mac": string
}
```
`device-access-token` to string wygenerowany przez backend i zwrócony w procesie [preinicjalizacji](#14-preinicjalizacja-urządzenia) do aplikacji.<br>
Paramter `mac` to adres MAC urządzenia zapisany jako string w formacie `xx:xx:xx:xx:xx:xx`.

Serwer -> Klient:<br>
Kod HTTP: `201` - Ok.<br>
Kod HTTP: `404` - Brak urządzenia w bazie spełniającego żądane kryteria.<br>

## 3.2. Zapis pomiarów
EP: `device/save-measure`
<br><br>
Klient -> Serwer:<br>
Metoda: `POST`<br>
Dodatkowy nagłówek:<br>
```HTTP
device-access-token: string
```
Dane:
```json
{
  "temperature": float,
  "humidity": float,
  "pm2_5": float,
  "pm10": float
}
```
`device-access-token` to string wygenerowany przez backend i zwrócony w procesie [preinicjalizacji](#14-preinicjalizacja-urządzenia) do aplikacji.<br>
Serwer -> Klient:<br>
Kod HTTP: `201` - Ok.<br>
Kod HTTP: `404` - Brak urządzenia w bazie spełniającego żądane kryteria.<br>

# 4. Statusy HTTP
- `401` - Gdy EP wymaga dodatkowych nagłówków HTTP, a klient ich nie wyśle.
- `403` - Gdy EP otrzyma błędny/wygasły token (np. użytkownik wylogowany)
- `406` - Gdy EP wymaga określonych danych wejściowych, a klient ich nie wyśle.
