# Nagłówki plików
Zaleca się używanie znormalizowanego formatu dla komentarzy umieszczanych w nagłówkach plików. 

Proces ten można zautomatyzować używając rozszerzenia do Visual Studio Code, np. "File Header Comment". Po zainstalowaniu należy udać się do ustawień i wyszukać ustawienie "fileHeaderComment.template". Następnie otworzyć plik "settings.json" oraz dodać następujące obiekty w globalnym obiekcie ustawień:
```json
{
    "fileHeaderComment.parameter":{
        "*":{
            "author": "Your name"
        }
    },
    "fileHeaderComment.template":{
        "tm-proj":[
            "${commentbegin}",
            "${commentprefix} @file\t\t${filename}", 
            "${commentprefix} @brief\t\ttype_brief_here",
            "${commentprefix} @author\t${author}", 
            "${commentprefix} @date\t\t${year}-${month}-${day}",
            "${commentprefix}",
            "${commentprefix} @note",
            "${commentprefix} Weahter station project", 
            "${commentprefix} Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala",
            "${commentprefix} Copyright (c) ${year} by Silesian University of Technology",
            "${commentend}"
        ]
    }
}
```